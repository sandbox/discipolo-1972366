api = 2
core = 7.x
projects[bean][version] = 1.2
projects[bean][subdir] = contrib

projects[ctools][version] = 1.3
projects[ctools][subdir] = contrib

projects[features][download][type] = "git"
projects[features][download][url] = "http://git.drupal.org/project/features.git"
projects[features][download][branch] = "7.x-2.x"
projects[features][subdir] = contrib

projects[field_slideshow][version] = 1.82
projects[field_slideshow][patch][] = http://drupal.org/files/field_slideshow-1228266-62.patch
projects[field_slideshow][subdir] = contrib

libraries[jquery.cycle][download][type] = get
libraries[jquery.cycle][download][url] = http://jquery.malsup.com/cycle/release/jquery.cycle.zip
libraries[jquery.cycle][destination] = libraries

