<?php
/**
 * @file
 * bean_slidefields.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function bean_slidefields_user_default_permissions() {
  $permissions = array();

  // Exported permission: create any bean_slidefield bean.
  $permissions['create any bean_slidefield bean'] = array(
    'name' => 'create any bean_slidefield bean',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site manager' => 'site manager',
    ),
    'module' => 'bean',
  );

  // Exported permission: delete any bean_slidefield bean.
  $permissions['delete any bean_slidefield bean'] = array(
    'name' => 'delete any bean_slidefield bean',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site manager' => 'site manager',
    ),
    'module' => 'bean',
  );

  // Exported permission: edit any bean_slidefield bean.
  $permissions['edit any bean_slidefield bean'] = array(
    'name' => 'edit any bean_slidefield bean',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site manager' => 'site manager',
    ),
    'module' => 'bean',
  );

  // Exported permission: view any bean_slidefield bean.
  $permissions['view any bean_slidefield bean'] = array(
    'name' => 'view any bean_slidefield bean',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'editor' => 'editor',
      'site manager' => 'site manager',
    ),
    'module' => 'bean',
  );

  return $permissions;
}
