<?php
/**
 * @file
 * bean_slidefields.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function bean_slidefields_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'bean_slidefield';
  $bean_type->label = 'bean_slidefield';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['bean_slidefield'] = $bean_type;

  return $export;
}
